<?php

namespace Drupal\cron_queue_invoker;

use Cron\CronExpression;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Manage populating queue with jobs automatically on cron.
 *
 * @todo Cache the applicable queue workers so we don't have to scan them all.
 */
class CronQueueManager {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * CronQueueManager constructor.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queueWorkerManager
   *   The queue worker manager.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(TimeInterface $time, StateInterface $state, QueueWorkerManagerInterface $queueWorkerManager, QueueFactory $queueFactory, LoggerChannelFactoryInterface $loggerFactory, ConfigFactoryInterface $configFactory) {
    $this->time = $time;
    $this->state = $state;
    $this->queueWorkerManager = $queueWorkerManager;
    $this->queueFactory = $queueFactory;
    $this->loggerFactory = $loggerFactory;
    $this->configFactory = $configFactory;
  }

  /**
   * Queue items on according to their schedule.
   *
   * @param array|null $plugin_ids
   *   Optionally specific queue worker plugin IDs we want to trigger on
   *   schedule.
   */
  public function onSchedule(?array $plugin_ids = NULL): void {
    $definitions = $this->queueWorkerManager->getDefinitions();

    // Filter by the provided plugin IDs, if any.
    if ($plugin_ids) {
      $definitions = array_filter($definitions, function (array $definition) use ($plugin_ids) {
        return in_array($definition['id'], $plugin_ids);
      });
    }

    // Filter by those that should be scheduled.
    $definitions = array_filter($definitions, [$this, 'shouldWorkerBeStarted']);

    // Create the queue items.
    $this->startQueueWorkers($definitions);
  }

  /**
   * Start the given queue workers.
   *
   * @param array[]|string[] $plugins
   *   Optionally specific queue worker plugin IDs or plugin definitions we want
   *   to trigger on schedule.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function startQueueWorkers(?array $plugins = NULL): void {
    if (is_null($plugins)) {
      $plugins = $this->queueWorkerManager->getDefinitions();
    }

    foreach ($plugins as $plugin) {
      // Ensure we have the plugin definition.
      if (!is_array($plugin)) {
        $plugin = $this->queueWorkerManager->getDefinition($plugin);
      }

      // Check the plugin should be invoked on cron.
      if (!$plugin['cron_invoke']) {
        continue;
      }

      // Add a queue item with the last run date.
      $this->queueFactory
        ->get($plugin['id'])
        ->createItem([$this->getLastRunTime($plugin), $this->getDateTime()]);

      // Set the last run time.
      $this->setLastRunTime($plugin);
    }
  }

  /**
   * Check whether a queue worker should be started according to it's schedule.
   *
   * @param array $definition
   *   The plugin definition.
   *
   * @return bool
   *   Whether an item should be queued.
   *
   * @throws \Exception
   *   If the interval defined in the cron queue is invalid.
   *
   * @todo Support intervals in the format supported by \DateInterval().
   */
  protected function shouldWorkerBeStarted(array $definition): bool {
    // Check the plugin should be invoked on cron.
    if (!$definition['cron_invoke']) {
      return FALSE;
    }

    $cron = NULL;
    if (!empty($definition['cron_invoke']['cron'])) {
      try {
        $cron = new CronExpression($definition['cron_invoke']['cron']);
      }
      catch (\InvalidArgumentException $e) {
        $this->loggerFactory->get('cron_queue_invoker')->warning(
          'Invalid cron expression (' . $definition['cron_invoke']['cron'] . ') provided in ' . $definition['id']
        );
        return FALSE;
      }
    }

    // If cron available, conditions take precedence over the rest.
    // If none of cron and last run is available, then invoke immediately.
    $last_run = $this->getLastRunTime($definition);
    if ($cron) {
      // Compare if the previous cron run time is greater than the last
      // execution and less or equal than the request time.
      // When there's no last execution time, use the time of the last time cron
      // ran, this should be the case only when the queue is new and has not
      // been processed before.
      $last_run_timestamp = $last_run ? $last_run->getTimestamp() : $this->state->get('system.cron_last', 0);
      $previous_cron_run_timestamp = $cron->getPreviousRunDate($this->getDateTime()->getPhpDateTime(), 0, TRUE)->getTimestamp();
      return $previous_cron_run_timestamp > $last_run_timestamp &&
        $previous_cron_run_timestamp <= $this->time->getCurrentTime();
    }
    elseif (!$last_run) {
      return TRUE;
    }

    // Consistently format our times based on the desired interval.
    $format = 'Y-m-d H:i:s';
    $split_pos = strpos($format, $definition['cron_invoke']['interval']);
    if ($split_pos === FALSE) {
      throw new \Exception('Invalid run interval.');
    }
    $run_format = trim(substr($format, 0, $split_pos + 1), '- ');

    // If we have a last run, check it was before today.
    if ($last_run->format($run_format) >= $this->getDateTime()->format($run_format)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get the last run time for a queue worker.
   *
   * @param array $definition
   *   The plugin definition.
   *
   * @return \Drupal\Component\Datetime\DateTimePlus|null
   *   The last run time, or NULL for never.
   */
  protected function getLastRunTime(array $definition): ?DateTimePlus {
    $state = $definition['cron_invoke']['state'];
    $last_run_timestamp = $this->state->get($state);
    return isset($last_run_timestamp) ? $this->getDateTime($last_run_timestamp) : NULL;
  }

  /**
   * Set the last run time for a queue worker.
   *
   * @param array $definition
   *   The plugin definition.
   */
  protected function setLastRunTime(array $definition): void {
    $state = $definition['cron_invoke']['state'];
    $this->state->set($state, $this->time->getCurrentTime());
  }

  /**
   * Get a DateTime object from a timestamp.
   *
   * This makes sure the site timezone is always used from config.
   *
   * @param int|null $timestamp
   *   The timestamp to convert into a date time object. If empty the current
   *   time from the time service will be used.
   *
   * @return \Drupal\Component\Datetime\DateTimePlus
   *   The date time object.
   */
  protected function getDateTime($timestamp = NULL) {
    if (!$timestamp) {
      $timestamp = $this->time->getCurrentTime();
    }

    $config = $this->configFactory->get('system.date');
    return DateTimePlus::createFromTimestamp($timestamp, $config->get('timezone.default') ?: NULL);
  }

}
