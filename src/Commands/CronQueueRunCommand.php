<?php

namespace Drupal\cron_queue_invoker\Commands;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Queue\RequeueException;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\cron_queue_invoker\CronQueueManager;
use Drush\Style\DrushStyle;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Command for running cron invoked queues.
 */
class CronQueueRunCommand extends Command {

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The cron queue manager.
   *
   * @var \Drupal\cron_queue_invoker\CronQueueManager
   */
  protected $cronQueueManager;

  /**
   * Construct the cron invoked queue run command.
   *
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   * @param \Drupal\cron_queue_invoker\CronQueueManager $cron_queue_manager
   *   The cron queue manager.
   */
  public function __construct(QueueWorkerManagerInterface $queue_worker_manager, QueueFactory $queue_factory, CronQueueManager $cron_queue_manager) {
    parent::__construct();
    $this->queueWorkerManager = $queue_worker_manager;
    $this->queueFactory = $queue_factory;
    $this->cronQueueManager = $cron_queue_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('cron-queues:run')
      ->setAliases(['cronq:r'])
      ->setDescription('Run the cron queues.')
      ->addArgument('name', InputArgument::OPTIONAL, 'The worker plugin ID to run. Multiple plugin ids can be comma separated.')
      ->addOption('always', 'a', InputOption::VALUE_NONE, 'Whether to always queue items.')
      ->addOption('no-process', NULL, InputOption::VALUE_NONE, 'Do not process the queues.');
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrushStyle($input, $output);

    $plugin_ids = $input->getArgument('name');
    $plugin_ids = $plugin_ids ? explode(',', $plugin_ids) : NULL;

    if ($input->getOption('always')) {
      $io->text('Starting workers regardless of schedule');
      $this->cronQueueManager->startQueueWorkers($plugin_ids);
    }
    else {
      $io->text('Starting workers according to schedule');
      $this->cronQueueManager->onSchedule($plugin_ids);
    }

    if ($input->getOption('no-process')) {
      return;
    }

    foreach ($this->queueWorkerManager->getDefinitions() as $definition) {
      if (!$definition['cron_invoke']) {
        continue;
      }

      $queue = $this->queueFactory->get($definition['id']);

      $item = $queue->claimItem();
      if (!$item) {
        $io->text("<info>Nothing to process for {$definition['title']}</info>");
        continue;
      }

      /** @var \Drupal\Core\Queue\QueueWorkerInterface $worker */
      $worker = $this->queueWorkerManager->createInstance($definition['id']);

      $io->text("Processing {$definition['title']}");
      $progress = $io->createProgressBar();
      $progress->display();
      do {
        try {
          $worker->processItem($item->data);
          $queue->deleteItem($item);
        }
        catch (RequeueException $e) {
          // The worker requested the task to be immediately requeued.
          $queue->releaseItem($item);
        }
        catch (SuspendQueueException $e) {
          // If the worker indicates there is a problem with the whole queue,
          // release the item.
          $queue->releaseItem($item);

          $progress->clear();
          $io->error($e->getMessage());
          $progress->display();
        }

        $progress->advance();
      } while ($item = $queue->claimItem());

      $progress->clear();
      $io->text("<info>Finished processing {$definition['title']}</info>");
    }
  }

}
