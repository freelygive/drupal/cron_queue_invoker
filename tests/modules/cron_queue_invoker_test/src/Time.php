<?php

namespace Drupal\cron_queue_invoker_test;

use Drupal\Component\Datetime\Time as CoreTime;

/**
 * Mock time service for test coverage.
 *
 * @package Drupal\cron_queue_invoker_test
 */
class Time extends CoreTime {

  /**
   * {@inheritdoc}
   */
  public function getRequestTime() {
    if ($mock_date = \Drupal::state()->get('cron_queue_invoker_test.mock_date', NULL)) {
      return $mock_date;
    }

    return parent::getRequestTime();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentTime() {
    if ($mock_date = \Drupal::state()->get('cron_queue_invoker_test.mock_date', NULL)) {
      return $mock_date;
    }

    return parent::getCurrentTime();
  }

}
