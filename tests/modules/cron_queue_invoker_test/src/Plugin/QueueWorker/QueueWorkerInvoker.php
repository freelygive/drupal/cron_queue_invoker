<?php

namespace Drupal\cron_queue_invoker_test\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Queue worker with interval setting.
 *
 * @QueueWorker(
 *   id = "cron_queue_invoker_test_cron",
 *   deriver = "\Drupal\cron_queue_invoker_test\Plugin\Deriver\QueueWorkerInvoker",
 * )
 */
class QueueWorkerInvoker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // Do nothing.
  }

}
