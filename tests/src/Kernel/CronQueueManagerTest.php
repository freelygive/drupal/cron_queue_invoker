<?php

namespace Drupal\Tests\cron_queue_invoker\Kernel;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\cron_queue_invoker\CronQueueManager;
use Drupal\cron_queue_invoker_test\Plugin\Deriver\QueueWorkerInvoker;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the cron queue manager.
 *
 * @group cron_queue_invoker_test
 */
class CronQueueManagerTest extends KernelTestBase {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Worker queues to test.
   *
   * @var \Drupal\Core\Queue\QueueInterface[]
   */
  protected $queues;

  /**
   * The logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'cron_queue_invoker',
    'cron_queue_invoker_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->time = $this->container->get('datetime.time');
    $this->state = $this->container->get('state');
    $this->queueWorkerManager = $this->container->get('plugin.manager.queue_worker');
    $this->queueFactory = $this->container->get('queue');
    $this->loggerFactory = $this->container->get('logger.factory');
    $this->configFactory = $this->container->get('config.factory');

    foreach (QueueWorkerInvoker::getDefaultDefinitions() as $id => $definition) {
      $this->queues[$id] = $this->queueFactory->get('cron_queue_invoker_test_cron:' . $id);
    }
  }

  /**
   * Test conditions to determine when to queue an item.
   *
   * There should be the following different queue workers:
   *   queue                      interval/cron   frequency
   *   no_invoker                 false           -
   *   cron_invalid_syntax        0 7 * * * *     Only 5 parameters allowed
   *   cron_invalid_expression    0 7 0 * *       0 is not a valid day of month
   *   interval_day               d               Once a day
   *   interval_month             m               Once a month
   *   cron_daily_morning:        0 7 * * *       Everyday at 07:00
   *   cron_daily_morning_night   30 7,19 * * *   Everyday at 07:30 and 19:30
   *   cron_weekdays              0 13 * * 1-5    Every weekday at 13:00
   *   cron_weekends              0 13 * * 6-7    Every weekend at 13:00
   *   cron_monthly               0 8 3 * *       Every 3rd of the month at 08
   *   cron_yearly                37 12 30 3 *    Every year on March 30th at 12
   *
   * @see \Drupal\cron_queue_invoker_test\Plugin\Deriver\QueueWorkerInvoker
   */
  public function testOnSchedule() {
    // Start testing assuming cron ran one hour before the first execution of
    // the queue manager.
    $this->state->set('system.cron_last', strtotime('march 1st 2021 00:00:00'));

    // Only interval queues are executed the first time.
    $this->executeCronQueueManager('march 1st 2021 01:00:00');
    $this->assertQueues(['interval_day', 'interval_month']);

    // After one hour, no new items are queued.
    $this->executeCronQueueManager('march 1st 2021 02:00:00');
    $this->assertQueues([]);

    // The cron_daily_morning should be scheduled at 07:00.
    $this->executeCronQueueManager('march 1st 2021 07:00:00');
    $this->assertQueues(['cron_daily_morning']);

    // The cron_daily_morning_night should be scheduled at 07:30.
    $this->executeCronQueueManager('march 1st 2021 07:30:00');
    $this->assertQueues(['cron_daily_morning_night']);

    // The cron_weekdays should be scheduled at 13:00.
    $this->executeCronQueueManager('march 1st 2021 13:00:00');
    $this->assertQueues(['cron_weekdays']);

    // No new items are queued after a couple of hours.
    $this->executeCronQueueManager('march 1st 2021 15:00:00');
    $this->assertQueues([]);

    // To end the day, the cron_daily_morning_night should be scheduled at
    // 20:00:00 hours.
    $this->executeCronQueueManager('march 1st 2021 20:00:00');
    $this->assertQueues(['cron_daily_morning_night']);

    // After three days, the first monthly queue should be executed, along with
    // the daily morning, daily morning night, the interval day, and the
    // weekdays.
    $this->executeCronQueueManager('march 3rd 2021 15:00:00');
    $this->assertQueues([
      'interval_day',
      'cron_daily_morning',
      'cron_daily_morning_night',
      'cron_weekdays',
      'cron_monthly',
    ]);

    // No new items are queued after a couple of hours.
    $this->executeCronQueueManager('march 3rd 2021 17:00:00');
    $this->assertQueues([]);

    // Daily morning night queue should be executed at 19:30.
    $this->executeCronQueueManager('march 3rd 2021 19:30:00');
    $this->assertQueues(['cron_daily_morning_night']);

    // At the end of the week, the weekends queue should be executed along with
    // interval day, cron daily morning, and cron daily morning night.
    // Also cron weekdays should be executed but only because the cron hasn't
    // been executed in three days.
    $this->executeCronQueueManager('march 6th 2021 14:00:00');
    $this->assertQueues([
      'cron_weekends',
      'interval_day',
      'cron_daily_morning',
      'cron_daily_morning_night',
      'cron_weekdays',
    ]);

    // The yearly cron queue should be executed on march 30th. Also, interval
    // day, daily morning, daily morning night, weekdays, and weekends should be
    // executed. Some of those is because cron didn't run in several days.
    $this->executeCronQueueManager('march 30th 2021 13:00:00');
    $this->assertQueues([
      'cron_yearly',
      'cron_weekends',
      'cron_weekdays',
      'interval_day',
      'cron_daily_morning',
      'cron_daily_morning_night',
    ]);

    // One month later interval month should be executed. And a bunch of daily
    // queues due to cron not running in weeks.
    $this->executeCronQueueManager('april 5th 2021 08:00:00');
    $this->assertQueues([
      'interval_month',
      'cron_monthly',
      'interval_day',
      'cron_weekends',
      'cron_weekdays',
      'cron_daily_morning',
      'cron_daily_morning_night',
    ]);

    // One year later, all queues should be executed except for the yearly one.
    $this->executeCronQueueManager('march 30th 2022 12:30:00');
    $this->assertQueues([
      'interval_month',
      'cron_monthly',
      'interval_day',
      'cron_weekends',
      'cron_weekdays',
      'cron_daily_morning',
      'cron_daily_morning_night',
    ]);

    // The cron yearly should be now executed.
    $this->executeCronQueueManager('march 30th 2022 12:37:00');
    $this->assertQueues(['cron_yearly']);
  }

  /**
   * Execute the cron queue manager.
   *
   * Set the current time to the given date, creates a new instance of the
   * CronQueueManager so the time change makes effect, and process the queues
   * by calling the onSchedule method inside the manager.
   *
   * @param string $date
   *   The string representation of the time to execute.
   */
  protected function executeCronQueueManager(string $date) {
    $this->state->set('cron_queue_invoker_test.mock_date', strtotime($date));
    $cron_queue_manager = new CronQueueManager(
      $this->time,
      $this->state,
      $this->queueWorkerManager,
      $this->queueFactory,
      $this->loggerFactory,
      $this->configFactory
    );
    $cron_queue_manager->onSchedule();
  }

  /**
   * Run assertions on the queues.
   *
   * Loop over the queues stored in queues properties and asserts that the
   * number of items is either 0 or 1, this will be calculated based on the
   * active_queues array that is passed.
   *
   * @param string[] $active_queues
   *   The id of the queues that are expected to be active.
   */
  protected function assertQueues(array $active_queues) {
    $date = DateTimePlus::createFromTimestamp(
      $this->state->get('cron_queue_invoker_test.mock_date')
    )->format('Y-m-d H:i:s');

    foreach ($this->queues as $id => $queue) {
      $active = (int) in_array($id, $active_queues);
      $this->assertEquals(
        $active,
        $queue->numberOfItems(),
        'Date: ' . $date . ', Queue: ' . $id
      );
      if ($active) {
        $queue->deleteQueue();
      }
    }
  }

}
