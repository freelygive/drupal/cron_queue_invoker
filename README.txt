Cron Queue Invoker

Important!
=========

Make sure your cron is set to run frequently. If you want a cron queue to run
every minute your drupal cron must run at least once a minute.
